﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;
    
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("GameManager: Error");
            }
            return _instance;
        }
    }

    [System.Serializable]
    public class BurstConfig
    {
        public float bombRate = 0.5f;

        public float bombSpeed = 3.0f;

        public float speed = 3.0f;

        public uint bombCount = 1;

        public uint pointsPerBomb = 1;
    }
    public BurstConfig[] bursts;
    uint currentBurst = 0;
    private uint lives = 3;

    void Awake()
    {
        if (_instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    void OnDestroy()
    {
        if (this == _instance)
        {
            _instance = null;
        }
    }
    public void NextBurst()
    {
        if (GameObject.FindGameObjectWithTag("Bomb"))
        {
            Debug.LogWarning("Hay bombas en el juego");
            return;
        }

        if (lives == 0)
        {
            lives = 3;
            GetPlayer().serNumBars(lives);
            currentBurst = 0;
        }

        EnemyController ec = GetEnemy();
        if (ec == null)
        {
            return;
        }
        if (currentBurst == bursts.Length)
        {
            --currentBurst;
        }
        ec.bombRate = bursts[currentBurst].bombRate;
        ec.bombSpeed = bursts[currentBurst].bombSpeed;
        ec.speed = bursts[currentBurst].speed;
        ec.bombCount = bursts[currentBurst].bombCount;
        ++currentBurst;
        ec.StartBombing();
    }
    static PlayerController GetPlayer()
    {
        GameObject player;
        player = GameObject.FindGameObjectWithTag("Player");
        if (player != null)
        {
            return player.GetComponent<PlayerController>();
        }
        else
        {
            return null;
        }
    }
    public void BombGrounded()
    {
        foreach (GameObject go in GameObject.FindGameObjectsWithTag("Bomb"))
        {
            BombController bc = go.GetComponent<BombController>();
            if (bc != null)
            {
                bc.Explode();
            }
        }
        EnemyController ec = GetEnemy();
        ec.StopBombing();

        --currentBurst;
        if (currentBurst > 0)
        {
            --currentBurst;
        }

        --lives;
        PlayerController player = GetPlayer();
        if (player != null)
        {
            player.setNumBars(lives);
        }
    }
}
