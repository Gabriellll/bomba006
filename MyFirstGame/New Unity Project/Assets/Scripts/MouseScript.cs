﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseScript : MonoBehaviour
{
    [Tooltip("Mínima x durante el desplazamiento")]
    public float xMin = -10f;
    [Tooltip("Máxima x durante el desplazamiento")]
    public float xMax = 10f;

    private float accMouseMovement = 0.0f;

    private Rigidbody rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (rb == null)
        {
            enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        accMouseMovement += Input.GetAxisRaw("Mouse X");
    }
    void FixedUpdate()
    {
        Vector3 currentPos = transform.position;
        currentPos.x += accMouseMovement;
        accMouseMovement = 0.0f;
        if (currentPos.x < xMin)
        {
            currentPos.x = xMin;
        }
        else if (currentPos.x > xMax)
        {
            currentPos.x = xMax;
        }
        //transform.position = currentPos;
        rb.MovePosition(currentPos);
        //rb.position(currentPos);
    }
}
